#pragma once
#include <iostream>
#include <iomanip>


using namespace std;
class MyVector
{
private:
	int length;
	double* vector;
public:
	MyVector() : length(0), vector(nullptr) {};
	explicit MyVector(int _length) : length(_length)
	{
		vector = new double[length];
	}
	MyVector(int _length, double _filler) : length(_length)
	{
		vector = new double[length];
		for (int i = 0; i < _length; i++)
			vector[i] = _filler;
	}
	MyVector(const MyVector& _vector)
	{
		length = _vector.length;
		vector = new double[length];
		for (int i = 0; i < length; i++)
			vector[i] = _vector[i];
	}

	double& operator[](int i);
	const double& operator[](int i) const;
	MyVector& operator+=(const MyVector& _vector);
	MyVector& operator-=(const MyVector& _vector);
	MyVector& operator*=(const double a);
	MyVector& operator=(const MyVector& _vector);

	int getLength() { return length; }
	void setRandom(double min, double max);
	MyVector setLength(int _length);
	void printSelf(ostream& stream) const;

	~MyVector()
	{
		if (length > 0)
			delete[] vector;
		vector = nullptr;
	}
};


double& MyVector :: operator[](int i)
{
	if (i < 0 || i >= length)
	{
		cout << "Error: vector index is out of range!";
		exit(EXIT_FAILURE);
	}
	else
	{
		return vector[i];
	}
}

const double& MyVector :: operator[](int i) const
{
	if (i < 0 || i >= length)
	{
		cout << "Error: vector index is out of range!";
		exit(EXIT_FAILURE);
	}
	else
	{
		return vector[i];
	}
}

MyVector& MyVector::operator+=(const MyVector& _vector)
{
	if (_vector.length != length)
	{
		cout << "Error: vectors have different length!";
		exit(EXIT_FAILURE);
	}
	else
		for (int i = 0; i < length; i++)
			vector[i] += _vector[i];
	return *this;
}

MyVector& MyVector::operator-=(const MyVector& _vector)
{
	if (_vector.length != length)
	{
		cout << "Error: vectors have different length!";
		exit(EXIT_FAILURE);
	}
	else
		for (int i = 0; i < length; i++)
			vector[i] -= _vector[i];
	return *this;
}

MyVector& MyVector::operator*=(const double a)
{
	for (int i = 0; i < length; i++)
		vector[i] *= a;
	return *this;
}

MyVector& MyVector::operator=(const MyVector& _vector)
{
	if (this != &_vector)
	{
		if (length > 0)
			delete[] vector;
		length = _vector.length;
		vector = new double[length];
		for (int i = 0; i < length; i++)
			vector[i] = _vector[i];
	}
	return *this;
}

MyVector operator+(const MyVector& v1, const MyVector& v2)
{
	MyVector tmp(v1);
	tmp += v2;
	return tmp;
}

MyVector operator-(const MyVector& v1, const MyVector& v2)
{
	MyVector tmp(v1);
	tmp += v2;
	return tmp;
}

MyVector operator*(MyVector& v, const double a)
{
	MyVector tmp(v);
	tmp *= a;
	return tmp;
}

void MyVector::printSelf(ostream& stream) const
{
	for (int i = 0; i < length; i++)
	{
		stream << setfill(' ') << setw(15) << this->vector[i];
	}
}

void MyVector:: setRandom(double min, double max)
{
	for (int i = 0; i < length; i++)
		vector[i] = ((double)rand() / RAND_MAX) * (max - min) + min;
}