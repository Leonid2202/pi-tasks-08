#include <iostream>
#include <fstream>
#include "MyVector.h"
#include "Matrix.h"

int main()
{
	Matrix mat1, mat2, mat3, mat4(5,5);
	ifstream inputFile("input_data.txt");
	if (!inputFile.is_open())
	{
		cout << "Error: can't open input_data.txt!";
		exit(EXIT_FAILURE);
	}
	//��������� 2 ������� �� �����
	mat1.readFromFile(inputFile);
	mat2.readFromFile(inputFile);
	inputFile.close();
	//������� ��������� ������� �� �������
	cout << "A:\n" << mat1 << "\n";
	cout << "B:\n" << mat2 << "\n";
	//��������� �������������� �������� � ���������
	mat1 += mat1*5;
	cout << "A + A * 5:\n" << mat1 << "\n";
	mat3 = mat2 * mat1;
	cout << "B * A:\n" << mat3 << "\n";
	//��������� ���������� �������� ������� �� �������� ���������������
	//(������������ ������� �� �������� ����� ��������� �������)
	mat3 = Matrix(4, 4);
	mat3.setRandom(0, 100);
	cout << "C:\n" << mat3 << "\n";
	cout << "C * C^-1:\n" << mat3 * mat3.invert() << "\n";

	//���������� ��������� ������� � ���������� ���������� � ��� � ����
	ofstream outputFile("output_data.txt");
	if (!outputFile.is_open())
	{
		cout << "Error: can't open output_data.txt!";
		exit(EXIT_FAILURE);
	}
	mat4.setRandom(0, 20);
	mat4.writeToFile(outputFile);
	outputFile.close();
}

