#pragma once
#include <iostream>
#include <fstream>
#include "MyVector.h"


using namespace std;
class Matrix
{
private:
	int nRows, nColumns;
	MyVector* matrix;
public:
	Matrix() : nColumns(0), nRows(0),  matrix(nullptr) {};
	Matrix(int n, int m) : nColumns(n), nRows(m)
	{
		matrix = new MyVector[m];
		for (int i = 0; i < m; i++)
			matrix[i] = MyVector(n);
	}
	Matrix(int n, int m, double _filler) : nColumns(n), nRows(m)
	{
		matrix = new MyVector[m];
		for (int i = 0; i < m; i++)
			matrix[i] = MyVector(n,_filler);
	}
	Matrix(const Matrix& _matrix)
	{
		if (nRows > 0)
			delete[] matrix;
		nRows = _matrix.nRows;
		nColumns = _matrix.nColumns;
		matrix = new MyVector[nRows];
		for (int i = 0; i < nRows; i++)
		{
			matrix[i] = _matrix[i];
		}
	}

	friend ostream& operator<<(ostream& stream, const Matrix& m);

	bool operator==(const Matrix& m) const;
	bool operator!=(const Matrix& m) const;

	MyVector& operator[](int i);
	const MyVector& operator[](int i) const;
	Matrix& operator+=(const Matrix& _matrix);
	Matrix& operator-=(const Matrix& _matrix);
	Matrix& operator*=(const Matrix& _matrix);
	Matrix& operator*=(const double a);
	Matrix& operator/=(const double a);
	Matrix& operator=(const Matrix& _matrix);

	int getRows() const { return nRows; }
	int getColumns() const { return nColumns; }

	void setRandom(double min, double max);
	Matrix transpose();
	double computeDeterminant();
	Matrix Matrix::findMinor(int _n, int _m);
	Matrix invert();

	void readFromFile(ifstream& inputFile);
	void writeToFile(ofstream& outputFile);

	~Matrix()
	{
		if (matrix)
			delete[] matrix;
		matrix = nullptr;
	}
};

ostream& operator<<(ostream& stream, const Matrix& m)
{
	for (int i = 0; i < m.getRows(); i++)
	{
		stream << "||";
		m[i].printSelf(stream);
		stream << "||\n";
	}
	return stream;
}

bool Matrix::operator==(const Matrix& m) const
{
	if ((nColumns != m.getColumns()) & (nRows != m.getRows()))
		return 0;
	for (int i = 0; i < nRows; i++)
		for (int j = 0; j < nColumns; j++)
			if (matrix[i][j] != m[i][j])
				return 0;
	return 1;
}

bool Matrix::operator!=(const Matrix& m) const
{
	return ! (*this == m);
}

MyVector& Matrix::operator[](int i)
{
	if (i < 0 || i >= nRows)
	{
		cout << "Error: index is out of range!";
		exit(EXIT_FAILURE);
	}
	else
	{
		return matrix[i];
	}
}

const MyVector& Matrix::operator[](int i) const
{
	if (i < 0 || i >= nRows)
	{
		cout << "Error: index is out of range!";
		exit(EXIT_FAILURE);
	}
	else
	{
		return matrix[i];
	}
}

Matrix& Matrix::operator+=(const Matrix& _matrix)
{
	if (nRows != _matrix.nRows || nColumns != _matrix.nColumns)
	{
		cout << "Error: matrices have different sizes!";
		exit(EXIT_FAILURE);
	}
	else
		for (int i = 0; i < nRows; i++)
			matrix[i] += _matrix[i];
	return *this;
}

Matrix& Matrix::operator-=(const Matrix& _matrix)
{
	if (nRows != _matrix.nRows || nColumns != _matrix.nColumns)
	{
		cout << "Error: matrices have different sizes!";
		exit(EXIT_FAILURE);
	}
	else
		for (int i = 0; i < nRows; i++)
			matrix[i] -= _matrix[i];
	return *this;
}

Matrix& Matrix::operator*=(const Matrix& _matrix)
{
	if (this->getColumns() != _matrix.getRows())
	{
		cout << "Error:  the number of columns of the left matrix is not the same as the number of rows of the right matrix!";
		exit(EXIT_FAILURE);
	}
	else
	{
		Matrix tmp(_matrix.getColumns(), this->getRows(), 0);
		for (int i = 0; i < tmp.getRows(); i++)
			for (int j = 0; j < tmp.getColumns(); j++)
				for (int k = 0; k < _matrix.getRows(); k++)
					tmp[i][j] += (*this)[i][k] * _matrix[k][j];
		*this = tmp;
	}
	return *this;
}

Matrix& Matrix::operator*=(const double a)
{
	for (int i = 0; i < nRows; i++)
			matrix[i] *= a;
	return *this;
}

Matrix& Matrix::operator/=(const double a)
{
	for (int i = 0; i < nRows; i++)
		matrix[i] *= 1/a;
	return *this;
}

Matrix& Matrix::operator=(const Matrix& _matrix)
{
	if (this != &_matrix)
	{
		if (nRows > 0)
			delete[] matrix;
		nRows = _matrix.nRows;
		nColumns = _matrix.nColumns;
		matrix = new MyVector[nRows];
		for (int mi = 0; mi < nRows; mi++)
		{
			matrix[mi] = _matrix[mi];
		}
	}
	return *this;
}

Matrix operator+(const Matrix& m1, const Matrix& m2)
{
	Matrix tmp(m1);
	tmp += m2;
	return tmp;
}

Matrix operator-(const Matrix& m1, const Matrix& m2)
{
	Matrix tmp(m1);
	tmp -= m2;
	return tmp;
}

Matrix operator*(const Matrix& m1, const Matrix& m2)
{
	if (m1.getColumns() != m2.getRows())
	{
		cout << "Error:  the number of columns of the left matrix is not the same as the number of rows of the right matrix!";
		exit(EXIT_FAILURE);
	}
	else
	{
		Matrix tmp(m2.getColumns(), m1.getRows(), 0);
		for (int i = 0; i < tmp.getRows(); i++)
			for (int j = 0; j < tmp.getColumns(); j++)
				for (int k = 0; k < m2.getRows(); k++)
					tmp[i][j] += m1[i][k] * m2[k][j];
		return tmp;
	}
}

Matrix operator*(const Matrix& m, const double a)
{
	Matrix tmp(m);
	for (int i = 0; i < m.getRows(); i++)
		tmp[i] *= a;
	return tmp;
}

Matrix operator/(const Matrix& m, const double a)
{
	Matrix tmp(m);
	for (int i = 0; i < m.getRows(); i++)
		tmp[i] *= 1/a;
	return tmp;
}

void Matrix::setRandom(double min, double max)
{
	for (int i = 0; i < nRows; i++)
		matrix[i].setRandom(min, max);
}

Matrix Matrix::transpose()
{
	Matrix tmp(nRows, nColumns);
	for (int i = 0; i < tmp.getRows(); i++)
		for (int j = 0; j < tmp.getColumns(); j++)
		{
			tmp[i][j] = matrix[j][i];
		}
	return tmp;
}

double Matrix::computeDeterminant()
{
	if (nRows != nColumns)
	{
		cout << "Error: current matrix is not a square matrix!";
		exit(EXIT_FAILURE);
	}
	if (nRows == 1)
		return matrix[0][0];
	if (nRows == 2)
		return (matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]);
	Matrix tmp(*this);
	double res = 1;
	for (int i = 0; i < nColumns; i++)
	{
		if (tmp[i][i] == 0)
		{
			int j = i + 1;
			while (j < nColumns)
				if (tmp[j++][i] != 0)
					break;
			if (tmp[j-1][i] == 0)
				return 0;
			else
				tmp[i] += tmp[j-1];
		}
		for (int j = i + 1; j < nColumns; j++)
		{
			tmp[j] -= tmp[i] * (tmp[j][i] / tmp[i][i]);
		}
		res *= tmp[i][i];
	}
	return res;
}

Matrix Matrix::findMinor(int _n, int _m)
{
	if (_n < 0 || _m < 0 || _n > nColumns || _m > nRows)
	{
		cout << "Error: index is out of range!";
		exit(EXIT_FAILURE);
	}
	Matrix tmp(nColumns - 1, nRows - 1);
	int i = 0, j = 0;
	int ti = 0, tj = 0;
	while (i < nColumns)
	{
		if (i == _n)
			i++;
		if (i == nColumns)
			break;
		j = 0;
		tj = 0;
		while (j < nRows)
		{

			if (j == _m)
				j++;
			if (j == nRows)
				break;
			tmp[tj++][ti] = matrix[j++][i];
		}
		ti++;
		i++;
	}
	return tmp;
}

Matrix Matrix::invert()
{
	if (this->computeDeterminant() == 0)
	{
		cout << "Error: can't invert matrix, determinant is 0!";
		exit(EXIT_FAILURE);
	}
	else
	{
		Matrix tmp(nColumns, nRows);
		for (int i = 0; i < nColumns; i++)
			for (int j = 0; j < nRows; j++)
				tmp[j][i] = pow((-1), i + j) * this->findMinor(j, i).computeDeterminant();
		tmp.transpose();
		Matrix res = tmp / (this->computeDeterminant());
		return res;
	}
}

void Matrix::readFromFile(ifstream& inputFile)
{

	int n, m;
	inputFile >> n >> m;
	Matrix mat(n, m);
	for (int j = 0; j < m; j++)
		for (int i = 0; i < n; i++)
			inputFile >> mat[j][i];
	*this = mat;
}

void Matrix::writeToFile(ofstream& outputFile)
{
	outputFile << "Original matrix:\n";
	outputFile << *this;
	outputFile << "\nDeterminant = " << this->computeDeterminant() << "\n\n";
	outputFile << "Inverted matrix:\n";
	outputFile << this->invert();
}